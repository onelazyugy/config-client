package com.viet.le.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class MessageController {
    @Value("${msg:Hello world - Config Server is not working..pelase check}")
    private String msg;

    @Value("${test}")
    private String test;

    @RequestMapping("/msg")
    String getMsg() {
        return this.msg;
    }

    @RequestMapping("/test")
    String getTest() {
        return this.test;
    }
}
